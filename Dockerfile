FROM alpine:latest
RUN set -euxo pipefail
RUN apk update && apk add --no-cache ca-certificates
RUN wget -q -O /usr/local/bin/irma https://github.com/privacybydesign/irmago/releases/download/v0.4.1/irma-master-linux-amd64
RUN chmod +x /usr/local/bin/irma
# RUN echo "f74bbafef1b37b954f233adb526c67cf3005b2fa9585cd0f8246054a85a5e7ba /usr/local/bin/irma" | sha256sum -c

COPY ./config ./config
CMD ["sh", "-c", "/usr/local/bin/irma server -vv --config ./config/irmaserver.json --url $BASE_URL"]

EXPOSE 8088
